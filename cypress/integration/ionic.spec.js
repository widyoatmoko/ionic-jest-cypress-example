describe('The Login Page', function () {
    it('successfully loads', function () {
        cy.visit('/')
    })

    it('should redirect to login page', function () {
        cy.url().should('include', '/login')
    })

    it('should focus at email input', function () {
        cy.focused().should('have.attr', 'name', 'email')
    })

    it('should display empty email error', function () {
        cy.get('input[name=email]').clear().blur()
        cy.get('.error').should('contain', 'Please input your email')
    })

    it('should display invalid email error', function () {
        cy.get('input[name=email]').clear().type('user').blur()
        cy.get('.error').should('contain', 'Email address invalid')
    })

    it('should display empty password error', function () {
        cy.get('input[name=password]').clear().blur()
        cy.get('.error').should('contain', 'Please input your password')
    })

    it('should display minlength password error', function () {
        cy.get('input[name=password]').clear().type('123456').blur()
        cy.get('.error').should('contain', 'Minimum 8 characters')
    })

    it('should display maxlength password error', function () {
        cy.get('input[name=password]').clear().type('12345678901234').blur()
        cy.get('.error').should('contain', 'Maximum 12 characters')
    })

    it('check with valid input value', function () {
        cy.get('input[name=email]').clear().type('user@localhost').should('have.value', 'user@localhost')
        cy.get('input[name=password]').clear().type('12345678').should('have.value', '12345678')
        cy.get('ion-button').click()
    })

    it('should redirect to page home', function () {
        cy.url().should('include', '/home')
    })

})

describe('The Home Page', function () {
    it('successfully loads', function () {
        cy.url().should('include', '/home')
    })

    it('should display alert', function () {
        cy.get('ion-alert').should('be.visible')
    })

    it('successsfully close alert', function () {
        cy.wait(500)
        cy.get('button').click()
        cy.wait(500)
        cy.get('ion-alert').should('not.be.visible')
    })

})