# Ionic4 Project With Jest & Cypress Testing Framework

1. Install dependencies:
```
npm i
```
2. Run jest unit test:
```
ng test --verbose
```
3. Run cypress e2e test:
```
npm run test:e2e
```