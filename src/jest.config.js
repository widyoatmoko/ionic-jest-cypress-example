module.exports = {
  preset: 'jest-preset-angular',
  transformIgnorePatterns: [
    '<rootDir>/node_modules/(?!@ionic|rxjs-compat)',
  ],
  testPathIgnorePatterns: [
    '<rootDir>/cypress/',
    '<rootDir>/ionic-mocks-jest/'
  ],
  coveragePathIgnorePatterns: [
    '<rootDir>/cypress/',
    '<rootDir>/ionic-mocks-jest/'
  ],
};
