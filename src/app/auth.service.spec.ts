import { TestBed, inject } from "@angular/core/testing";

import { AuthService } from "./auth.service";
import { Storage } from "@ionic/storage";
import { StorageMock } from "ionic-mocks-jest";
import { HttpClient, HttpClientModule } from "@angular/common/http";

describe("AuthService", () => {
  let storage: Storage;
  let http: HttpClient;
  let authService: AuthService;

  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        { provide: Storage, useFactory: () => StorageMock.instance() },
        AuthService,
        HttpClient
      ]
    })
  );

  it("should be created", () => {
    const service: AuthService = TestBed.get(AuthService);
    expect(service).toBeTruthy();
  });

  it("should login successfully", async () => {
    const loginData: any = {
      email: "user@localhost",
      password: "12345678"
    };
    const service: AuthService = TestBed.get(AuthService);
    await expect(service.login(loginData)).resolves.toBeTruthy();
  });

  it("should login unsuccessfully", async () => {
    const loginData: any = {
      email: "user@localhost",
      password: "wrong password"
    };
    const service: AuthService = TestBed.get(AuthService);
    await expect(service.login(loginData)).rejects.toBeFalsy();
  });

  it("should get todo list successfully", async () => {
    const service: AuthService = TestBed.get(AuthService);
    await expect(service.getTodoList().toPromise()).resolves.toMatchSnapshot();
  });
});
