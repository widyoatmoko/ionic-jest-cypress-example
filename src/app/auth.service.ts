import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  AUTH_URL = "http://digischool.waikiindonesia.com/api/api-token-auth/";
  TODO_URL = "https://jsonplaceholder.typicode.com/todos/"
  // TODO_URL = "https://jsonplaceholder.typicode.com/users/"

  constructor(
    private storage: Storage,
    private http: HttpClient
  ) { }

  isAuth() {
    this.storage.get('token').then((val) =>{
      if(val) {
        return true;
      } else {
        return false;
      }
    })
  }

  login(data) {
    let loginData:any={};
    loginData.nisn = data.email;
    loginData.password = data.password;
    return new Promise((resolve, reject) => {
      this.http.post(this.AUTH_URL, loginData)
      .subscribe((res) => {
        // console.log(res);
        this.storage.set('token', res);
        return resolve(res);
      }, err => {
        return reject(false);
      })
    });
  }

  getTodoList() {
    let httpUrl = this.TODO_URL;
    // let httpUrl = this.BASE_URL;
    return this.http.get(httpUrl);
  }

  logout() {
    this.storage.remove('key')
    return true;
  }
}
