import { AuthService } from './../auth.service';
import { Router } from '@angular/router';
import { ReactiveFormsModule, FormsModule, FormBuilder } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA, NgModuleFactoryLoader } from '@angular/core';
import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';

import { RouterTestingModule } from "@angular/router/testing";

import { LoginPage } from './login.page';
import { IonicModule } from '@ionic/angular';
import { Location } from '@angular/common';
import { Storage } from '@ionic/storage';
import { StorageMock } from 'ionic-mocks-jest';
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";

// import { routes, AppRoutingModule } from "../app-routing.module";
import { HomePageModule } from '../home/home.module';


describe('LoginPage', () => {
  let component: LoginPage;
  let fixture: ComponentFixture<LoginPage>;
  let location: Location;
  let router: Router;
  let authService: AuthService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ 
        RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
        IonicModule,
        HomePageModule,
        HttpClientTestingModule
      ],
      declarations: [ LoginPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: Storage, useFactory: () => StorageMock.instance() },
        FormBuilder,
        AuthService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    router = TestBed.get(Router);
    location = TestBed.get(Location);

    // router.initialNavigation();
    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.ngOnInit();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(fixture).toMatchSnapshot();
  });

  it('form valid', () => {
    let email = component.loginForm.controls['email'];
    email.setValue('thisis@validemail.com')
    let password = component.loginForm.controls['password'];
    password.setValue('1234abcd')
    expect(component.loginForm.valid).toBeTruthy();
  });

  it('form invalid when empty', () => {
    expect(component.loginForm.valid).toBeFalsy();
  });

  it('form invalid when email field is empty', () => {
    let password = component.loginForm.controls['password'];
    password.setValue('1234abcd')
    expect(component.loginForm.valid).toBeFalsy();
  });

  it('form invalid when password field is empty', () => {
    let email = component.loginForm.controls['email'];
    email.setValue('thisis@validemail.com')
    expect(component.loginForm.valid).toBeFalsy();
  });

  it('email field validity: empty', () => {
    let errors = {};
    let email = component.loginForm.controls['email'];
    errors = email.errors || {};
    expect(errors['required']).toBeTruthy(); (1)
  });

  it('email field validity: wrong format', () => {
    let errors = {};
    let email = component.loginForm.controls['email'];
    email.setValue('invalidEmail')
    errors = email.errors || {};
    expect(errors['email']).toBeTruthy(); (1)
  });

  it('email field validity: max length error', () => {
    let errors = {};
    let email = component.loginForm.controls['email'];
    email.setValue('longformatemail@verylongformatemailthatmorethan30char.com')
    errors = email.errors || {};
    expect(errors['maxlength']).toBeTruthy(); (1)
  });

  it('password field validity: empty', () => {
    let errors = {};
    let password = component.loginForm.controls['password'];
    errors = password.errors || {};
    expect(errors['required']).toBeTruthy(); (1)
  });

  it('password field validity: minlength error', () => {
    let errors = {};
    let password = component.loginForm.controls['password'];
    password.setValue('1234')
    errors = password.errors || {};
    expect(errors['minlength']).toBeTruthy(); (1)
  });

  it('password field validity: maxlength error', () => {
    let errors = {};
    let password = component.loginForm.controls['password'];
    password.setValue('ThisPasswordIsTooLong')
    errors = password.errors || {};
    expect(errors['maxlength']).toBeTruthy(); (1)
  });

  it('call auth service on login', () => {
    const login = jest.spyOn(AuthService.prototype, 'login');
    component.doLogin();
    expect(login).toHaveBeenCalled();
  })

});
