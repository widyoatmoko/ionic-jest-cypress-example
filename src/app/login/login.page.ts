import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})


export class LoginPage implements OnInit {
  @ViewChild('email') emailInput;
  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.maxLength(30), Validators.email, Validators.required])],
      password: ['', Validators.compose([Validators.minLength(8), Validators.maxLength(12), Validators.required])]
    });
  }

  ionViewDidEnter() {
    setTimeout(() => {
      this.emailInput.setFocus();
    }, 500);
  }

  doLogin() {
    this.authService.login(this.loginForm.value)
    .then(() => {
      console.log('login berhasil');
      this.router.navigate(['/home']);
    }, (err) => {
      console.log('login gagal')
    });
  }
  
}
