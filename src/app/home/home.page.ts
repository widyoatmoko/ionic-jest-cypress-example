import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular'
import { Storage } from '@ionic/storage';

export interface Todo {
  userId?: number;
  id?: number;
  title?: string;
  completed?: boolean;
}

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  todos: any;

  constructor(public alertCtrl: AlertController, private storage: Storage, private authService: AuthService) {
    this.presentAlert();
  }

  ngOnInit() {
    this.authService.getTodoList().subscribe(data => {
      this.todos = data;
    });
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
      subHeader: 'Subheader',
      message: 'This is an alert message.',
      buttons: ['OK']
    });

    await alert.present();
  }

  _saveToken(token) {
    return this.storage.set('token', token);
  }

  _getToken() {
    return this.storage.get('token')
  }

}
