import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AuthService } from './../auth.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomePage } from './home.page';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage'
import { AlertControllerMock, AlertMock, StorageMock } from 'ionic-mocks-jest';
import { Observable, of } from 'rxjs';
// jest.mock('./home.page')
jest.mock('@ionic/storage');


// const mockStorage = StorageMock;
// jest.mock('@ionic/storage', () => {
//   return jest.fn().mockImplementation(() => {
//     return {Storage: mockStorage};
//   });
// })

describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;
  let alertCtrl: AlertController;
  let alert: HTMLIonAlertElement;
  let storage: Storage;
  let authService: AuthService;

  let classUnderTest: HomePage;
  let TODO_OBJECT  = [
    {
    "userId": 1,
    "id": 1,
    "title": "delectus aut autem",
    "completed": false
    },
    {
    "userId": 1,
    "id": 2,
    "title": "quis ut nam facilis et officia qui",
    "completed": false
    },
    {
    "userId": 1,
    "id": 3,
    "title": "fugiat veniam minus",
    "completed": false
    }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [HomePage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: AlertController, useFactory: () => AlertControllerMock.instance() },
        { provide: Storage, useFactory: () => StorageMock.instance() },
        AuthService
      ]
        // { provide: Alert, useFactory: () => AlertMock.instance() }
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();

    alertCtrl = AlertControllerMock.instance(alert);
    alert = AlertMock.instance();
    storage = StorageMock.instance();

    classUnderTest = new HomePage(alertCtrl, storage, authService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call alert create', () => {
    component.presentAlert();
    // classUnderTest.presentAlert();
    expect(alertCtrl.create).toHaveBeenCalled();
  });

  it('should call alert present', async () => {
    // classUnderTest.presentAlert();
    component.presentAlert();
    const alert = await alertCtrl.create();
    expect(alert.present).toHaveBeenCalled();
  });

  it('should call storage set', async () => {
    const tokenKey = 'token';
    const tokenValue = '123456';
    storage = StorageMock.instance(tokenKey, tokenValue);
    classUnderTest = new HomePage(alertCtrl, storage, authService);

    await classUnderTest._saveToken(tokenValue);
    expect(storage.set).toHaveBeenCalledWith('token', tokenValue);
    const token = await storage.get(tokenKey);
    expect(token).toEqual(tokenValue);
  })

  it('should call storage get', async () => {
    storage = StorageMock.instance('token', '123456');
    classUnderTest = new HomePage(alertCtrl, storage, authService);

    const token = await classUnderTest._getToken();
    expect(storage.get).toHaveBeenCalledWith('token');
    expect(token).toEqual('123456');
  })

  it('should get todo list successfully', async () => {
    const todo = spyOn(AuthService.prototype, 'getTodoList').and.returnValue(of(TODO_OBJECT)); //mock todolist service

    component.ngOnInit();
    expect(todo).toHaveBeenCalled();
    // expect(component.todos).toMatchSnapshot();
    expect(component.todos).toEqual(TODO_OBJECT);
  })
});
